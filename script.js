//Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
//коли отримаємо дінні із сервера або від користувача, коли маємо потенційно уязвиму конструкцію та не хочемо щоб подальший код сторінки зупинився при помилці у скріпту.  


//Завдіння:
const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];


function makeError (arr) {
  arr.forEach(item => {
    try {    
      if (!item.author) throw `У книзі "${item.name}" відсутне ім'я автора`;
      if (!item.name) throw `У книзі автора ${item.author} відсутня назва книги`;
      if (!item.price) throw `У книзі автора ${item.author}:"${item.name}" відсутня ціна`;
            
    } catch (Error) {
      console.log('Error:'+ Error + ".")
    }})
}

makeError(books);

let params = ['author', 'name', 'price']

function isArrsEqual(array1, array2) {
  if (array1.length !== array2.length) {
    return false;
  }
  for (let i = 0; i < array1.length; i++) {
    if (array1[i] !== array2[i]) {
      return false;
    }
  }
  return true;
}

function validArr(arr, params) {
  return arr.filter(obj => {
    const keys = Object.keys(obj);
    return isArrsEqual(keys, params);
  });
}

let arr = (validArr(books, params));
let booksHolder = document.getElementById("root");
let list = document.createElement("ul");

function renderList (arr, list, docElement) {
  arr.forEach(item => {
  list.insertAdjacentHTML('beforeend',`<li><b>Автор:${item.author}</b><br>Назва:${item.name}<br>Ціна:${item.price}</li>`);
  })
  return docElement.appendChild(list);
}

renderList (arr, list, booksHolder);